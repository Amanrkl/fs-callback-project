const fs = require("fs");
const path = require("path");


function problem1() {

    console.log("Creating directory started");

    const dirPath = path.join(__dirname, "/randomJsonFiles");

    fs.mkdir(dirPath, { recursive: true }, (err) => {
        if (err) {
            console.error(err);
        } else {
            console.log("Successfully created directory");

            console.log("Starting json files creation");
            const noOfJSONFiles = Math.ceil(Math.random() * 10);
            const filenamesSuceedCreation = []
            const filenamesFailedCreation = []
            for (let fileIndex = 1; fileIndex <= noOfJSONFiles; fileIndex++) {
                let fileName = `${fileIndex}_file.json`;
                let filePath = path.join(dirPath, fileName);
                let fileData = {
                    RandomNumber: Math.random()
                };

                console.log(`Starting ${fileName} creation`);

                fs.writeFile(filePath, JSON.stringify(fileData), (err) => {
                    if (err) {
                        console.error(`Error when creating file ${fileName}`, err);
                        filenamesFailedCreation.push(fileName)
                    } else {
                        console.log(`Successfully created file ${fileName}`);
                        filenamesSuceedCreation.push(fileName)
                    }
                    if (filenamesFailedCreation.length + filenamesSuceedCreation.length == noOfJSONFiles) {
                        console.log("All files Created");

                        console.log("Starting Deletion");
                        for (let fileName of filenamesSuceedCreation) {
                            let filePath = path.join(dirPath, fileName)

                            console.log(`Starting ${fileName} deletion`);
                            fs.unlink(filePath, (err) => {
                                if (err) {
                                    console.error(`Error when deleting file ${fileName}`, err);
                                } else {
                                    console.log(`Successfully deleted file ${fileName}`);
                                }
                            });
                        }
                    }
                });
            }
        }
    });

}


module.exports = problem1