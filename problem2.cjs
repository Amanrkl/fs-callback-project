/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs');
const path = require('path');

function problem2() {
    const filePath = path.join(__dirname, '/lipsum.txt');
    console.log("Starting to read lipsum.txt");
    fs.readFile(filePath, "utf-8", (err, data) => {
        if (err) {
            console.error("Error in reading lipsum.txt", err);
        } else {
            console.log("Successful in reading lipsum.txt");
            const upperFileName = "upperCaselipsum.txt";
            fs.writeFile(path.join(__dirname, upperFileName), data.toUpperCase(), (err) => {
                if (err) {
                    console.error(err);
                } else {
                    fs.writeFile(path.join(__dirname, '/filenames.txt'), upperFileName, (err) => {
                        if (err) {
                            console.error(`Error while storing filename ${upperFileName}`, err);
                        }
                        else {
                            console.log(`Successful in storing filename ${upperFileName}`);
                            fs.readFile(path.join(__dirname, upperFileName), "utf-8", (err, data) => {
                                if (err) {
                                    console.error(err);
                                } else {
                                    const updatedData = data.toLowerCase().replaceAll("\n", "").replaceAll(". ", ".").split(".").join('.\n')
                                    const lowerFileName = "lowerCaselipsum.txt";
                                    fs.writeFile(path.join(__dirname, lowerFileName), updatedData, (err) => {
                                        if (err) {
                                            console.error(err);
                                        } else {
                                            fs.appendFile(path.join(__dirname, '/filenames.txt'), ", " + lowerFileName, (err) => {
                                                if (err) {
                                                    console.error(`Error while storing filename ${lowerFileName}`, err);
                                                } else {
                                                    console.log(`Successful in storing filename ${lowerFileName}`);
                                                    fs.readFile(path.join(__dirname, lowerFileName), "utf-8", (err, data) => {
                                                        if (err) {
                                                            console.error(err);
                                                        } else {
                                                            const sortedData = data.split("\n").sort().join("\n")
                                                            const sortedFileName = "sortedCaselipsum.txt";
                                                            fs.writeFile(path.join(__dirname, sortedFileName), sortedData, (err) => {
                                                                if (err) {
                                                                    console.error(err);
                                                                } else {
                                                                    fs.appendFile(path.join(__dirname, '/filenames.txt'), ", " + sortedFileName, (err) => {
                                                                        if (err) {
                                                                            console.log(`Error while storing filename ${sortedFileName}`, err);
                                                                        } else {
                                                                            console.log(`Successful in storing filename ${sortedFileName}`);
                                                                        }
                                                                        fs.readFile(path.join(__dirname, '/filenames.txt'), "utf-8", (err, data) => {
                                                                            if (err) {
                                                                                console.error('Error while reading filenames.txt ', err);
                                                                            } else {
                                                                                console.error('Successful in reading filenames.txt ');
                                                                                const fileNames = data.split(", ");
                                                                                for (let fileName of fileNames) {
                                                                                    console.log(`Starting ${fileName} deletion`);
                                                                                    fs.unlink(path.join(__dirname, fileName), (err) => {
                                                                                        if (err) {
                                                                                            console.error(`Error when deleting file ${fileName}`, err);
                                                                                        } else {
                                                                                            console.log(`Successfully deleted file ${fileName}`);
                                                                                        }
                                                                                    });
                                                                                }
                                                                            }
                                                                        });
                                                                    })
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

module.exports = problem2